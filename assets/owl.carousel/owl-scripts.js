jQuery(document).ready(function ($) {
    // xử lý khi:
    // + số item <=3 thì kg cho loop
    // + số item >3 thì cho loop
    // ------------ owl slide logo footer # fpt
      /*
    var loops;
    var count_item = $("._item_logo").length;
    console.log(count_item + ' item');
    var widthsc = $(window).width();
    if (count_item <= 3 && widthsc >= 1024) {
        loops = false;
    } else loops = true;
    "use strict";
    $('#logo-foot-slide').owlCarousel({
        loop: loops, // nếu đến item cuối cùng mà muốn trở lại item đầu thì dùng:  loop: loops, ngược lại: false
        center: false,
        items: 3,
        margin: 0,
        autoplay: false,
        dots: true,
        nav: true,
        autoplayTimeout: 9500, //8500
        smartSpeed: 250, //450
        navText: ['<i class="bi bi-chevron-left"></i>', '<i class="bi bi-chevron-right"></i>'],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 6
            },
            1400: {
                items: 8
            }
        }
    });
    var widthsc = $(window).width();
    if (count_item == 3 && widthsc > 1024) {
        $("#logo-foot-slide .owl-stage").addClass('add_mgl')
    }
    if (count_item <= 2 && widthsc > 1024) {
        $("#logo-foot-slide .owl-stage").addClass('add_mgl_2item')
    } else if (count_item >= 4 && widthsc <= 1024) {
        $("#logo-foot-slide .owl-stage").addClass('remove_mgl')
    }
    */
    // ------------ 

    // doi tac
    $('#owl_doitac').owlCarousel({
        loop: true,
        center: false,
        items: 3,
        margin: 10, // margin item
        autoplay: true,
        dots: true,
        nav: false,
        pagination: true,
        autoplayTimeout: 2500,//6500
        smartSpeed: 150,
        // navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],// neu nav: true thi mo ra
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });

    // du an
    $('#owl_duan').owlCarousel({
        loop: true,
        center: false,
        items: 3,
        margin: 20, // margin item
        autoplay: false,
        dots: true,
        nav: false,
        pagination: true,
        autoplayTimeout: 6500,
        smartSpeed: 450,
        // navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],// neu nav: true thi mo ra
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1024: {
                items: 3
            },
            1170: {
                items: 3
            }
        }
    });

});
$(document).ready(function () {
// v2 giai phap
    $('.owl__giaiphap').owlCarousel({
        loop: true,
        center: true,
        nav: true,
        dots: true,
        //   items: 5,
        margin: 10,
        navText: ['<i class="bi bi-chevron-left"></i>', '<i class="bi bi-chevron-right"></i>'],
        autoplay: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            },
            1024: {
                items: 5
            },
            1400: {
                items: 5
            }
        }
    })
});