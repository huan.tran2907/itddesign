(function ($) {
    $.fn.multitabs = function () {
        $('.tab__header > div').on('click', function () {
            var num;
            var _this = $(this);
            var classNameTab = _this.attr('class').split(' ');
            var classNameContent = _this.parent().siblings().children();

            for (i = 0; i <= classNameTab.length; i++) {
                if (/([\d.]+)/.test(classNameTab[i])) {
                    num = classNameTab[i].split('-')[1];
                    _this.siblings().removeClass('tab__header--active');
                    _this.addClass('tab__header--active');
                }
            }

            $(classNameContent).each(function (i, n) {
                var name = $(n).attr('class').split(' ');
                for (n = 0; n <= name.length; n++) {
                    if (name[n] != undefined) {
                        if (/([\d.]+)/.test(name[n])) {
                            if (num === name[n].split('-')[1]) {
                                var el = $(classNameContent)[num - 1];
                                $(classNameContent).removeClass('tab__content--active');
                                $(el).addClass('tab__content--active');
                            }
                        }
                    }
                }
            });
        });
    }
})(jQuery);
$('#tab_detail_1').multitabs();
$('#tab_detail_2').multitabs();
$('#tab_detail_3').multitabs();
$('#tab_detail_4').multitabs();
$('#tab_detail_5').multitabs();


// add class background when scroll > 500
$(window).scroll(function () {
    if ($(this).scrollTop() > 400) {
        $('.header_area').addClass('_js-bg-head')
    } else {
        $('.header_area').removeClass('_js-bg-head')
    }
});

//////
$(document).ready(function () {
    // https://stackoverflow.com/questions/26133618/how-to-stop-bootstrap-carousel-from-autosliding
    $('.custom-banner').carousel({
        interval : 10000, // 10s: https://stackoverflow.com/questions/17332431/how-can-i-control-the-speed-that-bootstrap-carousel-slides-in-items
        /*
        pause: true,
        interval: false
        */
    });


    // counters 
    const counters = document.querySelectorAll(".counter");
    counters.forEach((counter) => {
        counter.innerText = "0";
        const updateCounter = () => {
            const target = +counter.getAttribute("data-target");
            const count = +counter.innerText;
            const increment = target / 1000;
            if (count < target) {
                counter.innerText = `${Math.ceil(count + increment)}`;
                setTimeout(updateCounter, 1);
            } else counter.innerText = target;
        };
        updateCounter();
    });
});