//MENU ACTIVE CLASS & SCROLL
$(window).scroll(function(){
	if ($(window).scrollTop() > $('.header-nav-link').outerHeight()) {
		$('.nav-wrapper').addClass('fixed');
	} else {
		$('.nav-wrapper').removeClass('fixed');
	}
});

$(document).ready(function(){
    $('.nav_sticky a[href^="#"]').on('click',function (e) {
        e.preventDefault();
        var target = this.hash;
        var $target = $(target);
        var scroll;
        if($(window).scrollTop()==0){
            scroll =  ($target.offset().top) - 160// - 160
        }else{
            scroll =  ($target.offset().top) - 60// - 60 / - 160
        }
        $('html, body').stop().animate({
            'scrollTop': scroll
        }, 100, 'swing', function () {
            //window.location.hash = target;
        });
    });
});

$(window).on('scroll', function () {
   var sections = $('section.map_to_giaiphaps')
    , nav = $('.nav_sticky')
    , nav_height = nav.outerHeight()
    , cur_pos = $(this).scrollTop();
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
 
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('li a').removeClass('active');
      sections.removeClass('active');
 
      $(this).addClass('active');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});